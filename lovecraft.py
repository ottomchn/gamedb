import tsv
'''
data ripped of of wikipedia Great_Old_One page
from wiki "References: This field lists the stories in which the Great Old One makes a significant appearance or otherwise receives important mention. Sources are denoted by a simple two-letter code from the Cthulhu Mythos reference codes and bibliography and the Cthulhu Mythos alphanumeric reference code and bibliography. A code appearing in bold means that the story introduces the Great Old One. If the code is given as comics or rpg it means that the Great Old One first appeared in the Call of Cthulhu Role playing Game or are mentioned/depicted in comics rather than novels."

Name Epithets Description References
if no epithets the value is '-'
'''
def great_old_ones():
    return tsv.load('great_old_ones.tsv')

def great_ones():
    return tsv.load('great_ones.tsv')

def god_names(gods):
    r = []
    for g in gods:
        r += [g[0]]
    return r

def god_descs(gods):
    r = []
    for g in gods:
        r += [g[2]]
    return r

def god_epithets(gods):
    # sometimes people may come up with the same epithet so keep this list unique
    r = set()
    for g in gods:
        if ',' in g[1]:
            # this god has more than one epithet in a comma separated list
            for nn in g[1].split(', '):
                r.add(nn)
        elif g[1] != '-':
            # this god has a single epithet
            r.add(g[1])
    return list(r)

def db_info():
    import collections
    import os
    db_path = os.path.dirname(os.path.realpath(__file__)) + '/lovecraft/'
    dbs = [db_path+'great_ones.tsv', db_path+'great_old_ones.tsv']
    r = ''
    for db in dbs:
        # read data from .tsv
        data = tsv.load(db)
        # ensure the datadabe has no duplicate names
        names = god_names(data)
        assert [x for x, y in collections.Counter(names).items() if y > 1] == []
        r += "{}: {} records\n".format(db, len(data))
    return r[:-1]
 
