
def load(fn):
    ''' load data from .tsv file '''
    data = []
    f = open(fn, 'r')
    for l in f:
        data += [l.split('\t')]
    f.close()
    return data
